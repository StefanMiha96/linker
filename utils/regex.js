const rp = require('request-promise');

exports.findInString = function (string, patternString) {
    var regex = new RegExp(patternString, 'gs');
    
    let match = regex.exec(string);

    return match ? match[1] : null;    
};


exports.findAllInString = function (string, patternString) {
    let parts = [];
    var regex = new RegExp(patternString, 'gms');
    
    let match = regex.exec(string);
    while (match != null) {
        parts.push(match[1].trim());
        match = regex.exec(string);        
    }
    
    return parts;
};