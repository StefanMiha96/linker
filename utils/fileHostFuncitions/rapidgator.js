
const rp = require('request-promise');
const regEX = require('../../utils/regex');

    let continuez = false;
    let token;
    var options = {};
    let cookie;
    let html;
    let date = 0;
    let curentDate = 0;

exports.rapidgator =  async function (req,res) {

    //LOGIN 
    curentDate = new Date().getTime();
    if(curentDate > date + 21600000) {
  
      options = {
        method: 'POST',
        uri: 'https://rapidgator.net/api/v2/user/login?login=kosac50@gmail.com&password=Volimdaubijam1!',
        resolveWithFullResponse: true
      }
    
      html = await rp(options);
      token = JSON.parse(html.body);
      token = token.response.token;
      cookie = html.headers["set-cookie"][0];
      date = new Date().getTime();

    }
   
  
    // file link
    options = {
      method: 'GET',
      uri: req.session.data.fileLink,
      resolveWithFullResponse: true
    }
    html = await rp(options);
    let fid = regEX.findInString(html.body, 'var fid = (.+?);');
    let fileName = regEX.findInString(html.body, '<title>Download file (.+?)</title>');
    let fileIdPremium = '';
  
    //COMPY TO MY FILES
    options = {
      method: 'GET',
      uri: `https://rapidgator.net/download/AjaxCopyFile?fid=${fid}`,
      headers: {
        "Cookie": cookie,
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36",
        'X-Requested-With': 'XMLHttpRequest'
      },
      resolveWithFullResponse: true
    }
  
    html = await rp(options);
      html = JSON.parse(html.body)
  
    if (html.code != 7) {
      continuez = true
    }
  
    if (continuez) {
      // find id of file in my folder
      options = {
        method: 'GET',
        uri: `https://rapidgator.net/filesystem/index`,
        headers: {
          "Cookie": cookie,
          "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"
        },
        resolveWithFullResponse: true
      }
      html = await rp(options);
      let fileNamesIndirectory = regEX.findAllInString(html.body,'<td style="display:none">(.+?)<\/td');
      for(let i in fileNamesIndirectory){
        if(fileName == regEX.findInString(fileNamesIndirectory[i],'name&gt;(.+?),type&gt')){
          fileIdPremium =  regEX.findInString(fileNamesIndirectory[i],'id32&gt;(.+?),level&gt');
          break;
        }
  
      }
      // get premium link
      options = {
        method: 'POST',
        uri: `https://rapidgator.net/api/v2/file/onetimelink_create?token=${token}&file_id=${fileIdPremium}`,
        resolveWithFullResponse: true
      }
      html = await rp(options);
      let premiumLink = JSON.parse(html.body);
      premiumLink = premiumLink.response.link.url;
      let data = {};
      data.donloadFile = true;
      data.link = premiumLink;
      req.session.data.fileLink = '';
      req.session.data.download = false;
      req.session.data.dataToUser = data;
      return;

     } else {
  
      let ordinaryFileId = regEX.findInString(req.session.data.fileLink, 'file\/(.+?)\/');
      if(!ordinaryFileId) {
        ordinaryFileId = regEX.findInString(req.session.data.fileLink, 'file\/(.+?)(,|$)');  
      }
      options = {
        method: 'POST',
        uri: `https://rapidgator.net/api/v2/file/download?file_id=${ordinaryFileId}&token=${token}`,
        headers: {
          'X-Forwarded-For': `${req.session.data.country.ip}`
        },
        resolveWithFullResponse: true
      }
      html = await rp(options);
      premiumLink = JSON.parse(html.body);
      premiumLink = premiumLink.response.download_url;
      let data = {};
      data.donloadFile = true;
      data.link = premiumLink;
      req.session.data.fileLink = '';
      req.session.data.download = false;
      req.session.data.dataToUser = data;
      return;

     }
  
  }