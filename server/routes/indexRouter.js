let express = require('express');
let mysql = require('mysql');
let util = require('util');
let router = express.Router();
const rp = require('request-promise');
const regEX = require('../../utils/regex');
let uuid = require('uuid-random');
const puppeteer = require('puppeteer');
let fs = require('fs');
const path = require('path');
const EventEmitter = require('events');
let querystring = require('querystring');
const bus = new EventEmitter();
// filehosters //

let rapidgatorObj = require('../../utils/fileHostFuncitions/rapidgator');
const { json } = require('body-parser');
const { options } = require('request');
//const { delete } = require('request-promise');
//let countryTxt = require('../../utils/countrylist.txt');

let linkovi = "";
let linkoviHeader = "";
let fileHostingPrice = "";
let adsProviderMoneyCountry = "";
let fileHosters = "";
let countryData = {};
let hackToken;

let linkQue = 5;
let adsInOneLink = 5;
let gbDiscount = 10;
let profit = 60; // posto
//let gibPrice = 0.00533;
//let gibPrice = 0.00033;
let gibPrice = {};
let premiumApiObj = {};

let countryDataOldeTime = 0;
let sqlQueryOldTime = 0;
let linksTime = 0;
let userCheckTime = 0;


let browser;
let page = {};
let linksChrome = {};

let LinksPassword = 'Volimdaubijam1+'
let userName = 'Kosac';
let premiumApi = 'ivPBVfXign3oHcDwQGbJ'//-> za kosac96 //'BLu2G8PGuHHQCUSgx5Yl'

let linkObj = {};
let btn;
let hackTokenObj = {};

let pool = mysql.createPool({
  host: '127.0.0.1',
  user: 'root',
  password: 'root',
  database: 'linker',
  connectionLimit: 21,
  supportBigNumbers: true,
  bigNumberStrings: true
});

pool.query = util.promisify(pool.query);


let pool2 = mysql.createPool({
  host: '127.0.0.1',
  user: 'root',
  password: 'root',
  database: 'vpn',
  connectionLimit: 21,
  supportBigNumbers: true,
  bigNumberStrings: true
});

pool2.query = util.promisify(pool2.query);

startBrowser();


async function startBrowser() {
  browser = await puppeteer.launch({ headless: false });
  page = await browser.newPage();
  await page.setViewport({ width: 1920, height: 1080 });
  await page.setRequestInterception(true);
  page.on("request", interceptedRequest => {
    const url = interceptedRequest.url();

    if (url == 'https://ipstack.com/ipstack_js/scripts.ipstack.js') {
      interceptedRequest.respond({
        body: fs.readFileSync(path.resolve('utils', 'ipSkirpta.txt'), 'utf-8')
      });
    } else {
      interceptedRequest.continue();
    }
  });

  await page.goto('https://ipstack.com/', { waitUntil: 'load' });
  btn = await page.$x('/html/body/div[1]/section[1]');

  // OVO JE DA POTVRDI SERTIFIKAT
  let page2 = await browser.newPage();
  try {
    await page2.goto('https://109.198.1.59/', { waitUntil: 'load' });
  } catch (err) {
    await wait(1000);
    await page2.evaluate(() => document.querySelector('#proceed-link').click());
    await wait(2000);
    await page2.close();
  }

}

router.post('/deleteLink', async function (req, res) {
  req.session.admin = true;
  if (req.session.admin == true) {
    let mobile = req.body.mobile;
    let Linkproviders = req.body.provider;
    let linkoviData;
    let dataToFile = [];

    if (linkovi) {
      linkoviData = linkovi;
    } else {
      linkoviData = await pool.query('SELECT * FROM linker.linkovi');
      linkoviData = JSON.parse(linkoviData[0].data);
    }

    for (let i in linkoviData.links) {
      if (linkoviData.links[i].mobile == mobile && linkoviData.links[i].provider == Linkproviders) {
        delete linkoviData.links[i];
      }

    }
    // let linkoviIzFajla = require('../../utils/linkovi');
    // for(let i in linkoviIzFajla.links){
    //   if (linkoviIzFajla.links[i].mobile == mobile && linkoviIzFajla.links[i].provider == Linkproviders){
    //     delete linkoviIzFajla.links[i];
    //   }
    // }
    // fs.writeFile("./myfile.json", JSON.stringify(linkoviIzFajla, null, 4), function(err){
    //   //handle err, success
    // });
    linkovi = linkoviData;
    let date = new Date().getTime();
    data = JSON.stringify(linkoviData);
    await pool.query(`UPDATE linker.linkovi SET data = ?,date = ? where id = ?`, [data, date, 1]);
    res.send('finito');
  } else {
    return res.redirect('/');
  }


});




router.post('/linksinsert', async function (req, res) {
  req.session.admin = true;
  let mobile = req.body.mobile;
  let Linkproviders = req.body.provider;
  let found = null;
  let numOfLinks;
  if (req.session.admin == true) {
    let myLinks = regEX.findAllInString(req.body.message, '(.+?)\n');
    for (let i in myLinks) {
      if (myLinks[i] == '' || myLinks[i] == undefined) {
        delete myLinks[i];
      }
    }

    for (let i in linkObj) {
      if (linkObj[i].mobile == mobile && linkObj[i].provider == Linkproviders) {
        for (let j in myLinks) {
          linkObj[i].adsLinks[j].hashedLink = myLinks[j];
        }
      }
    }
    let data = await pool.query('SELECT * FROM linker.linkovi where id = ?', [1]);
    if (JSON.parse(data[0].data).links[1]) {
      data = JSON.parse(data[0].data);
      for (let i in data.links) {
        if (data.links[i].mobile == mobile && data.links[i].provider == Linkproviders) {
          for (let j in linkObj) {
            data.links[i] = linkObj[j];
            found = true;
          }

        }
        numOfLinks = i;
      }
      if (found == null) {
        for (let j in linkObj) {
          data.links[parseInt(numOfLinks) + 1] = linkObj[j];
        }

      }
      let date = new Date().getTime();
      data = JSON.stringify(data);
      linkObj = {};
      await pool.query(`UPDATE linker.linkovi SET data = ?,date = ? where id = ?`, [data, date, 1]);
    } else {
      data = {};
      data.links = {};
      for (let j in linkObj) {
        data.links[1] = linkObj[j];
      }
      // data.links[1] = linkObj[1];
      let date = new Date().getTime();
      data = JSON.stringify(data);
      linkObj = {};
      await pool.query('UPDATE linker.linkovi SET data = ?,date = ? where id = ?', [data, date, 1]);
    }
    return res.send('finito');
  } else {
    return res.redirect('/');
  }
});

router.get('/linksgenerator', async function (req, res) {
  req.session.admin = true;
  if (req.session.admin == true) {
    let linkoviHeaderIzFajla = require('../../utils/linkoviHeader');
    let providers = Object.keys(linkoviHeaderIzFajla.linksHeader);
    res.render('linkdashboard', { providers: providers });


  } else {
    return res.redirect('/');
  }
});

router.post('/getLink', async function (req, res) {
  //{"links": {"1": {"userid": "105e76e0396bf2670790e60c278966a0bbddb8ea", "adsLinks": {"0": {"secretKey": "71860", "hashedLink": "https://oko.sh/mUC0S"}}, "provider": "clk.sh"}
  req.session.admin = true;
  if (req.session.admin == true) {
    linkObj = {};
    let mobile = req.body.mobile;
    let Linkproviders = req.body.provider;
    let linkoviIzFajla = require('../../utils/linkovi');
    let linkoviHeaderIzFajla = require('../../utils/linkoviHeader');
    let secretKey;
    let linksToUser = {};
    let stringToUser = '';
    for (let i in linkoviIzFajla.links) {
      if (linkoviIzFajla.links[i].mobile == mobile && linkoviIzFajla.links[i].provider == Linkproviders) {

        linkObj[i] = linkoviIzFajla.links[i];
        linkObj[i].adsLinks = {}
        for (let j = 0; j < adsInOneLink; j++) {
          secretKey = await uuid().substring(0, 5);
          linkObj[i].adsLinks[j] = {};
          linkObj[i].adsLinks[j].secretKey = secretKey;
          linkObj[i].adsLinks[j].hashedLink = linkoviHeaderIzFajla.linksHeader[Linkproviders].mobile + secretKey;
          linksToUser[j] = {};
          linksToUser[j] = linkObj[i].adsLinks[j].hashedLink;
        }
      }
    }

    JSON.stringify(linkObj);
    for (let i in linksToUser) {
      stringToUser += linksToUser[i] + '\n';
    }
    return res.send(stringToUser);


  } else {
    return res.send('null');
  }
});


router.post('/adsLinks', async function (req, res) {
  let userPass = req.body.password;
  let userUss = req.body.username;
  if (userPass == LinksPassword && userUss == userName) {
    req.session.data.admin = true;
    return res.redirect('/linksgenerator');
  } else {
    return res.redirect('/');
  }
});



router.get('/ubaci_nove_podatke', async function (req, res) {

  if (req._remoteAddress == '127.0.0.1' || req._remoteAddress == '::1') {
    let fileHostersIzFajla = require('../../utils/fileHosters');
    let linkoviIzFajla = require('../../utils/linkovi');
    let linkoviHeaderIzFajla = require('../../utils/linkoviHeader');
    let fileHostingPriceIzFajla = require('../../utils/fileHostingPrice');
    let adsProviderMoneyCountryIzFajla = require('../../utils/adsProviderMoneyCountry');

    linkoviIzFajla = JSON.stringify(linkoviIzFajla);
    linkoviHeaderIzFajla = JSON.stringify(linkoviHeaderIzFajla);
    fileHostingPriceIzFajla = JSON.stringify(fileHostingPriceIzFajla);
    adsProviderMoneyCountryIzFajla = JSON.stringify(adsProviderMoneyCountryIzFajla);

    // await pool.query('UPDATE linker.linkovi SET data = ?', [linkoviIzFajla]);
    await pool.query('UPDATE linker.linkoviheader SET data = ?', [linkoviHeaderIzFajla]);
    await pool.query('UPDATE linker.filehostingprice SET data = ?', [fileHostingPriceIzFajla]);
    await pool.query('UPDATE linker.adsprovidermoneycountry SET data = ?', [adsProviderMoneyCountryIzFajla]);

    return res.send('GOTOVO');
  } else {
    return res.redirect('/');
  }

});

/* GET home page. */

router.get('/', async function (req, res) {


  let curentTime = new Date().getTime();


  if (sqlQueryOldTime + 540000 < curentTime) {
    linkovi = await pool.query('SELECT * FROM linker.linkovi where id = ?', [1]);
    linksTime = parseFloat(linkovi[0].date);
    linkovi = JSON.parse(linkovi[0].data);

    linkoviHeader = await pool.query('SELECT * FROM linker.linkoviheader');
    linkoviHeader = JSON.parse(linkoviHeader[0].data);

    fileHostingPrice = await pool.query('SELECT * FROM linker.filehostingprice');
    fileHostingPrice = JSON.parse(fileHostingPrice[0].data);

    adsProviderMoneyCountry = await pool.query('SELECT * FROM linker.adsprovidermoneycountry');
    adsProviderMoneyCountry = JSON.parse(adsProviderMoneyCountry[0].data);

    let data = await pool.query('SELECT * FROM linker.gb_price');
    for (let i in data) {
      gibPrice[data[i].name] = data[i];
    }

    data = await pool.query('SELECT * FROM linker.all_debrid');
    for (let i in data) {
      if (data[i].data) {
        data[i].data = JSON.parse(data[i].data);
      }
      premiumApiObj[i] = data[i];
    }




    sqlQueryOldTime = curentTime;
  }
  //  linksTime = 0
  // if (linksTime + 604800000 < curentTime) {
  //   generateLinks();
  // }

  if (countryDataOldeTime + 21600000 < curentTime) {
    countryData = await getCountryData();
  }
  if (userCheckTime + 86400000 < curentTime) {
    checkUsersIp(curentTime);
  }


  try {
    if (req.session.data.dataToUser) {

      if (req.session.data.download) {
        let data = {};
        data.fileName = req.session.data.dataToUser.fileName;
        data.canDonload = true;
        return res.render('dindex', { dataToUser: data });
      }

      return res.render('dindex', { dataToUser: req.session.data.dataToUser });
    } else {
      return res.render('dindex', { dataToUser: '' });
    }
  } catch (err) {
    if (err) {
      return res.render('dindex', { dataToUser: '' });
    }
  }

});


router.post('/link_check', async function (req, res) {

  //return res.redirect('/');


  if (req.body.link) {
    let curentTime = new Date().getTime();
    let dataToUser = {};
    let linkId;
    let adsProvider;
    let haveInDb = false;
    let locationData;
    let activelinkNum;
    let provider;
    let sizeGb;
    let fileName;

    // locationData = await getIpHack(req, res);
    // if(locationData == 'VPN'){
    //   delete req.session;
    //   return res.send('VPN');
    // }

    //linkovi = JSON.parse('{"links":{"1":{"userid":"105e76e0396bf2670790e60c278966a0bbddb8ea","provider":"clk.sh","adsLinks":{"0":{"secretKey":"3e2b9","hashedLink":"https://oko.sh/EvvW"},"1":{"secretKey":"7760d","hashedLink":"https://oko.sh/LKt6Qao"},"2":{"secretKey":"384d1","hashedLink":"https://oko.sh/xkVq"}}},"2":{"userid":"d67c585c173488142f67c5a200e0877614d60691","provider":"clk.sh","adsLinks":{"0":{"secretKey":"452d4","hashedLink":"https://oko.sh/hOfNjXs"},"1":{"secretKey":"3fd31","hashedLink":"https://oko.sh/9P5kGv"},"2":{"secretKey":"73169","hashedLink":"https://oko.sh/EElBrY2o"}}},"3":{"userid":"249ae4b2356374b4cde09461ef3e43a5","provider":"sh.st","adsLinks":{"0":{"secretKey":"f365f","hashedLink":"http://gestyy.com/w7jvVv?utm_source=&utm_medium=QL&utm_name=1"},"1":{"secretKey":"9f249","hashedLink":"http://gestyy.com/w7jvVW?utm_source=&utm_medium=QL&utm_name=1"},"2":{"secretKey":"85dfb","hashedLink":"http://gestyy.com/w7jvVU?utm_source=&utm_medium=QL&utm_name=1"}}},"8":{"userid":"d67c585c173488142f67c5a200e0877614d60691","provider":"clk.sh","adsLinks":{"0":{"secretKey":"ebd93","hashedLink":"https://oko.sh/OmgN5KD"},"1":{"secretKey":"b66a2","hashedLink":"https://oko.sh/Vt5jIRU"},"2":{"secretKey":"98f96","hashedLink":"https://oko.sh/xRtTQ7"}}},"9":{"userid":"d67c585c173488142f67c5a200e0877614d60691","provider":"clk.sh","adsLinks":{"0":{"secretKey":"40c24","hashedLink":"https://oko.sh/Imvi1h"},"1":{"secretKey":"c35c6","hashedLink":"https://oko.sh/WGxMBi"},"2":{"secretKey":"acd67","hashedLink":"https://oko.sh/rLYzH59D"}}},"10":{"userid":"105e76e0396bf2670790e60c278966a0bbddb8ea","provider":"clk.sh","adsLinks":{"0":{"secretKey":"a7cc4","hashedLink":"https://oko.sh/VeiTF"},"1":{"secretKey":"70851","hashedLink":"https://oko.sh/IM19SqOu"},"2":{"secretKey":"90f69","hashedLink":"https://oko.sh/z6n9rmm"}}},"11":{"userid":"105e76e0396bf2670790e60c278966a0bbddb8ea","provider":"clk.sh","adsLinks":{"0":{"secretKey":"c7b4a","hashedLink":"https://oko.sh/7qY18r"},"1":{"secretKey":"88142","hashedLink":"https://oko.sh/f9cf"},"2":{"secretKey":"79360","hashedLink":"https://oko.sh/5zxKyA4B"}}}}}')
    let options = await getAllDebridTestLink(req.body.link);
    html = await rp(options);
    if (html.body) {
      let htmlData = JSON.parse(html.body)
      provider = htmlData.data.infos[0].host;
      sizeGb = htmlData.data.infos[0].size / 1073741824;
      fileName = htmlData.data.infos[0].filename;
      let canProceed = await canProceedFunction(sizeGb, provider);
      if (!canProceed) {
        return res.send('nema');
      }

    }

    if (req.session.data) {
      if (req.session.data.country.ip != req._remoteAddress) {
        locationData = await getIpHack(req, res);
        if (locationData == 'VPN' || req.session.data.country.country != locationData.country) {
          req.session.data = '';
          return res.send('VPN');
        }
      }
    }
    if (!req.session.data) {
      let userDb = await pool.query('SELECT * FROM linker.user where ip = ?', [req._remoteAddress]);
      if (userDb.length == 0) {
        req.session.data = {};
        req.session.data.links = {};
        locationData = await getIpHack(req, res);
        if (locationData == 'VPN') {
          req.session.data = '';
          return res.send('VPN');
        }
      } else {
        haveInDb = true;
        req.session.data = JSON.parse(userDb[0].data);
        locationData = req.session.data.country;
      }
    } else {
      locationData = req.session.data.country;
      haveInDb = true;
    }
    // else {
    //   let userDb = await pool.query('SELECT * FROM linker.user where ip = ?', [req.session.data.country.ip]);
    //   if (userDb.length > 0) {
    //     haveInDb = true;
    //     req.session.data = JSON.parse(userDb[0].data);
    //     locationData = req.session.data.country;

    //   }

    // }

    // let sizeGb = await getGB(html, provider, req); old

    //req.session.data.country = locationData;
    req.session.data.fileProvoder = provider;
    req.session.data.sizeGb = sizeGb;
    req.session.data.fileName = fileName;
    req.session.data.download = false;

    if (!req.session.data.clickValue) {
      req.session.data.clickValue = {};
      req.session.data.clickValue.earnedMoney = 0;
    }
    let haveBendwithForDownload = await getAvaliableDownload(req.session.data);
    let canDonloadGb = 0;
    if (!gibPrice[req.session.data.fileProvoder]) {
      canDonloadGb = haveBendwithForDownload / parseFloat(gibPrice['default'].price);
    } else {
      canDonloadGb = haveBendwithForDownload / parseFloat(gibPrice[data.fileProvoder]);
    }
    // if (!gibPrice[req.session.data.fileProvoder]) {
    //   canDonloadGb = haveBendwithForDownload / gibPrice;
    // } else {
    //   canDonloadGb = haveBendwithForDownload / gibPrice[data.fileProvoder];
    // }
    if (canDonloadGb < req.session.data.sizeGb) {
      delete req.session;
      return res.send('nema');
    }


      //req.session.data.clickValue = {};
      //req.session.data.clickValue.earnedMoney = 0;

      activelinkNum = await userActiveLinksRandom(req); // KORISTI SE DA SE PROVERI DA LI USER IMA LINKOVE KOJIMA JE ISTEKO 24 H;
      if (activelinkNum.active > linkQue) {
        adsProvider = linkovi.links[activelinkNum.random].provider;
        linkId = activelinkNum.random;
        let subLinkKey = Object.keys(linkovi.links[linkId].adsLinks);
        let letSublinkRandom = Math.floor(Math.random() * subLinkKey.length);
        req.session.data.secretKey = linkovi.links[linkId].adsLinks[letSublinkRandom].secretKey;
        req.session.data.links[linkId].url = linkovi.links[linkId].adsLinks[letSublinkRandom].hashedLink;
      } else {
        linkId = await userRandomLinkId(req);
        if (linkId == 'error') {
          if (activelinkNum.error == true) {
            return res.send('nema vise');
          } else {
            linkId = activelinkNum.random;
          }
        }
        adsProvider = await linkSeter(req, linkId);
      }

      let clickValue = await getClickValue(locationData, adsProvider, req);
      let clickNum = await fileProvoderPrice(provider, sizeGb, clickValue, req);
      req.session.data.fileLink = req.body.link;

      // if (req.session.data.country.ip == '92.244.147.140') {
      //   req.session.data.download = true;clickValue:Object 
      //   await allDebrid(req);
      //   // req.session.data.download = true;
      //   // await Download(req, res);
      //   // // return res.send(req.session.data.dataToUser);
      //   return res.redirect('/');
      // }

      req.session.data.links[linkId].active = 1;
      dataToUser.link = req.session.data.links[linkId].url;
      dataToUser.clickNum = req.session.data.clickValue.earnedMoney / req.session.data.filePrice;
      dataToUser.fileName = req.session.data.fileName;
      req.session.data.dataToUser = dataToUser;
      compressed = JSON.stringify(req.session.data);

      if (haveInDb) {
        pool.query(`UPDATE linker.user SET data = ?, date = ? where ip = ?`, [compressed, curentTime, req.session.data.country.ip]);
      } else {
        pool.query('insert into linker.user values(null, ?,?,?)', [req.session.data.country.ip, compressed, curentTime]);
      }

      if (req.session.data.clickValue.earnedMoney > req.session.data.filePrice) {
        await allDebrid(req);
        let data = {};
        data.fileName = req.session.data.dataToUser.fileName;
        data.canDonload = true;
        return res.send(data);
      }
      return res.send(dataToUser);


    
  } else {
    return res.send('nemas za ovaj fajl');
  }

});

router.get('/token', async function (req, res) {
  if (req.query != '') {
    hackToken = req.originalUrl.replace('/token?', '');
    hackTokenObj[hackToken] = hackToken;
    hackToken = '';
    bus.emit('unlocked');
  }
  return res.send('');
});

async function getIpHack(req, res) {
  let ip = req.header('x-forwarded-for');
  let ipDb = await pool2.query('SELECT * FROM vpn.ip where ip = ?', [ip]);
  if (ipDb[0]) {
    let parsed = JSON.parse(ipDb[0].data);
    if (parsed.privacy.is_proxy || parsed.privacy.is_crawler || parsed.privacy.is_tor || parsed.privacy.tor || parsed.privacy.vpn || parsed.privacy.proxy || parsed.privacy.hosting) {
      return 'VPN';
    } else {
      parsed.ip = ip;
      req.session.data.country = parsed;
      return parsed;
    }
  }
  console.time("answer time");
  let ipHeader = await getIpHeader(ip);
  let vpnHeaderHtml = await rp(ipHeader);
  pool2.query('INSERT INTO vpn.ip values(null, ?,?)', [ip, vpnHeaderHtml.body]);
  vpnHeaderHtml = JSON.parse(vpnHeaderHtml.body);
  console.timeEnd("answer time");
  // console.time("answer time");
  // let geoHeadersOptions = await geoHeaders(ip);
  // let vpnHeadersOptions = await vpnHeaders(ip);
  // let smrda = rp(geoHeadersOptions);
  // let smrda1 = rp(geoHeadersOptions);
  // let smrda2 = rp(geoHeadersOptions);
  // let smrda3 = rp(geoHeadersOptions);
  // let smrda4 = rp(vpnHeadersOptions);
  // let smrda5 = rp(vpnHeadersOptions);
  // let geoHeadersHtml = rp(geoHeadersOptions);
  // let vpnHeaderHtml = rp(vpnHeadersOptions);
  // let allResults = await Promise.all([geoHeadersHtml, vpnHeaderHtml,smrda,smrda1,smrda2,smrda3,smrda4,smrda5]);
  // geoHeadersHtml = JSON.parse(allResults[0].body);
  // vpnHeaderHtml = JSON.parse(allResults[1].body);
  // geoHeadersHtml.security = vpnHeaderHtml;
  // pool2.query('INSERT INTO vpn.ip values(null, ?,?)', [ip, JSON.stringify(geoHeadersHtml)]);
  // console.timeEnd("answer time");

  // await btn[0].click();

  // let ipData = await new Promise(resolve => {
  //   bus.once('unlocked', async function () {
  //     let key = Object.keys(hackTokenObj);
  //     let currentToken = hackTokenObj[key[Math.floor(Math.random() * key.length)]];
  //     delete hackTokenObj[currentToken];
  //     options = await ipstackConf(ip, currentToken);
  //     html = await rp(options);
  //     data = JSON.parse(html.body);
  //     pool2.query('INSERT INTO vpn.ip values(null, ?,?)', [data.ip, html.body]);
  //     // try {
  //     //   await pool2.query('INSERT INTO vpn.ip values(null, ?,?)', [data.ip, html.body]);
  //     // } catch (err) {
  //     //   console.log(err);
  //     // }
  //     resolve(data);
  //   })

  // });
  // let keys = Object.keys(vpnHeaderHtml);
  // for (let i in keys) {
  //   if (vpnHeaderHtml[keys[i]] == true) {
  //     return 'VPN';
  //   }
  // }
  // geoHeadersHtml.ip = ip;
  // req.session.data.country = geoHeadersHtml;
  // return geoHeadersHtml;

  if (vpnHeaderHtml.privacy.proxy || vpnHeaderHtml.privacy.tor || vpnHeaderHtml.privacy.hosting || vpnHeaderHtml.privacy.vpn) {
    return 'VPN';
  } else {
    req.session.data.country = vpnHeaderHtml;
    return vpnHeaderHtml;
  }

}

async function ipstackConf(ip, hackToken) {
  let options = {
    method: 'POST',
    uri: `https://ipstack.com//ipstack_api.php?ip=${ip}&token=${hackToken}`,
    headers: {
      'X-Forwarded-For': `192.55.208.51`,
      'User-Agent': `Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36`,
      'Sec-Fetch-Site': `same-origin`,
      'Sec-Fetch-Mode': `cors`,
      'Sec-Fetch-Dest': `empty`,
      'Referer': `https://ipstack.com/`,
      'Host': `ipstack.com`,
      'Connection': `keep-alive`,
      'Accept': `application/json, text/javascript, */*; q=0.01`
    },
    resolveWithFullResponse: true
  }
  return options;
}


async function getIpHeader (ip) {

 let options = {
    method: 'GET',
    uri: `https://ipinfo.io/widget/${ip}`,
    headers: {
      'referer': 'https://ipinfo.io/',
      'X-Forwarded-For': `${ip}`,
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
    },
    resolveWithFullResponse: true
  }

  return options

}

async function vpnHeaders(ip, hackToken) {
  let form = {
    input: `${ip}`
  };
  let formData = querystring.stringify(form);
  let contentLength = formData.length;
 let options = {
    method: 'POST',
    uri: `https://ipinfo.io/proxy-vpn-detection-api`,
    body: formData,
    headers: {
      'Content-Length': contentLength,
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Forwarded-For': `${ip}`,
    },
    resolveWithFullResponse: true
  }
  return options;

}

async function geoHeaders(ip) {

  let form = {
    input: `${ip}`
  };
  let formData = querystring.stringify(form);
  let contentLength = formData.length;
  let options = {
    method: 'POST',
    uri: `https://ipinfo.io/ip-geolocation-api`,
    body: formData,
    headers: {
      'Content-Length': contentLength,
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-Forwarded-For': `${ip}`,
    },
    resolveWithFullResponse: true
  }
  return options;

}

router.get('/config/:id', async function (req, res) {
  if (req.session.data) {
    let dataToUser = {};
    let secretKey = req.params.id;
    let date = new Date().getTime();
    let linkId;
    let adsProvider;
    let locationData;
    let provider;
    let sizeGb;
    let curentTime = new Date().getTime();

    if (req.session.data.country.ip != req._remoteAddress) {
      locationData = await getIpHack(req, res);
      if (locationData == 'VPN' || req.session.data.country.country != locationData.country) {
        req.session.data = '';
        return res.send('VPN');
      }
    }

    if (secretKey == req.session.data.secretKey) {
      for (let i in req.session.data.links) {

        if (req.session.data.links[i].used == false && req.session.data.links[i].active == 1) {

          req.session.data.links[i].viewUsedTimes += 1;
          req.session.data.clickValue.earnedMoney += req.session.data.clickValue.cpm;

          if (req.session.data.links[i].viewTimes == req.session.data.links[i].viewUsedTimes) {
            req.session.data.links[i].used = true;
            req.session.data.links[i].time = date;
          }
        }
      }


      if (req.session.data.clickValue.earnedMoney > req.session.data.filePrice) {
        await allDebrid(req);
        pool.query(`UPDATE linker.user SET data = ? , date = ? where ip = ?`, [JSON.stringify(req.session.data), new Date().getTime(), req.session.data.country.ip]);
        return res.redirect('/');
        
      }


      let activelinkNum = await userActiveLinksRandom(req);
      if (activelinkNum.active > linkQue) { // OVO TREBA PROVERITI
        adsProvider = linkovi.links[activelinkNum.random].provider;
        linkId = activelinkNum.random;
        let subLinkKey = Object.keys(linkovi.links[linkId].adsLinks);
        let letSublinkRandom = Math.floor(Math.random() * subLinkKey.length);
        req.session.data.secretKey = linkovi.links[linkId].adsLinks[letSublinkRandom].secretKey;
        req.session.data.links[linkId].url = linkovi.links[linkId].adsLinks[letSublinkRandom].hashedLink;
      } else {
        linkId = await userRandomLinkId(req);
        if (linkId == 'error') {
          if (activelinkNum.error == true) {
            return res.send('nema vise');
          } else {
            linkId = activelinkNum.random;
          }

        }
        adsProvider = await linkSeter(req, linkId);
      }
      locationData = req.session.data.country;
      provider = req.session.data.fileProvoder;
      sizeGb = req.session.data.sizeGb;

      let clickValue = await getClickValue(locationData, adsProvider, req);
      let clickNum = await fileProvoderPrice(provider, sizeGb, clickValue, req);

      req.session.data.links[linkId].active = 1;
      dataToUser.link = req.session.data.links[linkId].url;
      dataToUser.clickNum = req.session.data.clickValue.earnedMoney / req.session.data.filePrice;
      dataToUser.fileName = req.session.data.dataToUser.fileName;
      req.session.data.dataToUser = dataToUser;

      let compressed = JSON.stringify(req.session.data);

      pool.query(`UPDATE linker.user SET data = ? , date = ? where ip = ?`, [compressed, curentTime, req.session.data.country.ip]);
      // return res.render('index3', { dataToUser: dataToUser });
      return res.redirect('/');


    } else {
      delete req.session.data;
      return res.redirect('/');
    }
  } else {
    return res.redirect('/');
  }

});

router.post('/download', async function (req, res) {
  if (req.session.data.download) {
    let link = req.session.data.premiumLink;
    req.session.data.download = false;
    delete req.session.data.clickValue;
    delete req.session.data.premiumLink;
    delete req.session.data.dataToUser;
    return res.send(link);
  } else {
    return res.redirect('/');
  }

});

async function Download(req, res) {

  if (req.session.data.download == true) {
    switch (req.session.data.fileProvoder) {
      case 'rapidgator.net':
        await rapidgatorObj.rapidgator(req, res);

    }

  } else {
    delete req.session.data;
    return res.redirect('/');
  }

}


async function getCountryData() {
  try {


    let ss = {};
    let keys = adsProviderMoneyCountry.data;
    require('fs').readFileSync(path.resolve('utils', 'countrylist.txt'), 'utf-8').split(/\r?\n/).forEach(function (line) {
      let code = regEX.findInString(line, ',(..)');
      let country = regEX.findInString(line, '(.+?),');
      ss[code] = {};
      ss[code].country = country;
      ss[code].cpm = {};
      for (let i in adsProviderMoneyCountry.data) {
        if (adsProviderMoneyCountry.data[i][country]) {
          ss[code].cpm[i] = adsProviderMoneyCountry.data[i][country].desktop;

        } else {
          ss[code].cpm[i] = adsProviderMoneyCountry.data[i]['Else'].desktop;
        }
      }
    });
    countryDataOld = ss;
    countryDataOldeTime = new Date().getTime();
    return ss;
  } catch (err) {
    console.log(err);
  }
}



async function getClickValue(locationData, adsProvider, req) {
  let myCpm = countryData[locationData.country].cpm[adsProvider];
  myCpm = (myCpm * profit) / 100;
  if (!req.session.data.clickValue) {
    req.session.data.clickValue = {};
    req.session.data.clickValue.cpm = myCpm;
    req.session.data.clickValue.earnedMoney = 0;
  } else {
    req.session.data.clickValue.cpm = myCpm;
  }
  return;
}

async function fileProvoderPrice(provider, sizeGb, clickValue, req) {
  let price = 0;
  if (!gibPrice[req.session.data.fileProvoder]) {
    price = parseFloat(gibPrice['default'].price);
  } else {
    price = parseFloat(gibPrice[data.fileProvoder]);
  }
  // let gibPrice = fileHostingPrice.fileHostingPrice[provider]; old
  let adsNum = 0;
  req.session.data.filePrice = sizeGb * price;
  return adsNum;
}

async function linkSeter(req, randomLinkId) {
  let subLinkKey = Object.keys(linkovi.links[randomLinkId].adsLinks);
  let letSublinkRandom = Math.floor(Math.random() * subLinkKey.length);

  if (req.session.data.links[randomLinkId]) {
    req.session.data.links[randomLinkId].url = linkovi.links[randomLinkId].adsLinks[letSublinkRandom].hashedLink;
    req.session.data.secretKey = linkovi.links[randomLinkId].adsLinks[letSublinkRandom].secretKey;
  } else {
    req.session.data.links[randomLinkId] = {};
    req.session.data.secretKey = linkovi.links[randomLinkId].adsLinks[letSublinkRandom].secretKey;
    req.session.data.links[randomLinkId].url = linkovi.links[randomLinkId].adsLinks[letSublinkRandom].hashedLink;
    req.session.data.links[randomLinkId].used = false;
    req.session.data.links[randomLinkId].viewTimes = linkoviHeader.linksHeader[linkovi.links[randomLinkId].provider].views;
    req.session.data.links[randomLinkId].viewUsedTimes = 0;
    req.session.data.links[randomLinkId].active = 0;
  }
  return linkovi.links[randomLinkId].provider;
}

async function getAvaliableDownload(data) {
  let sumOfPotentialMoney = 0;
  let potentionalLinks = {};
  let canDonloadGb = 0;
  for (let i in linkovi.links) {

    if (data.links[i]) {
      if (!data.links[i].used) {
        potentionalLinks[i] = {};
        potentionalLinks[i].Used = data.links[i].viewUsedTimes;
        potentionalLinks[i].canBeUsed = linkoviHeader.linksHeader[linkovi.links[i].provider].views;
        potentionalLinks[i].provider = linkovi.links[i].provider;
      }
    } else {
      potentionalLinks[i] = {};
      potentionalLinks[i].Used = 0;
      potentionalLinks[i].canBeUsed = linkoviHeader.linksHeader[linkovi.links[i].provider].views;
      potentionalLinks[i].provider = linkovi.links[i].provider
    }

  }
  for (let i in potentionalLinks) {
    let myCpm = countryData[data.country.country].cpm[potentionalLinks[i].provider];
    for (let j = potentionalLinks[i].Used; j <= potentionalLinks[i].canBeUsed; j++) {
      myCpm = (myCpm * profit) / 100;
      sumOfPotentialMoney += myCpm;
    }

  }
  // if(!gibPrice[data.fileProvoder]){
  //   canDonloadGb = sumOfPotentialMoney / gibPrice;
  // } else {
  //   canDonloadGb = sumOfPotentialMoney / gibPrice[data.fileProvoder];
  // }

  return sumOfPotentialMoney;


}



async function userActiveLinksRandom(req) { //Uzima random link koji vec postoji kod jusera;
  let date = new Date().getTime();
  let daymilisec = 86400000;
  let activeLinkPool = [];
  let ativeObj = {};
  for (let i in req.session.data.links) {
    req.session.data.links[i].active = 0;
    if (!linkovi.links[i]) {
      delete req.session.data.links[i];
    } else if (req.session.data.links[i].used == false) {
      activeLinkPool.push(i);
    } else if (req.session.data.links[i].time + daymilisec < date) { //  && req.session.data.links[i].used == true
      delete req.session.data.links[i];
    }
  }
  ativeObj.active = activeLinkPool.length;
  ativeObj.random = activeLinkPool[Math.floor(Math.random() * activeLinkPool.length)];
  if (ativeObj.active == 0) {
    ativeObj.error = true;
  }
  return ativeObj;
}


async function userRandomLinkId(req) { // Vraca linkove koje korisnik ne koristi
  let activeLinkPool = [];
  for (let j in linkovi.links) {

    if (req.session.data.links) {
      if (!req.session.data.links[j]) {
        activeLinkPool.push(j);
      }
    } else {
      activeLinkPool.push(j);
    }
  }
  if (activeLinkPool.length == 0) {
    return 'error';
  }

  return activeLinkPool[Math.floor(Math.random() * activeLinkPool.length)];
}

async function checkUsersIp(date) {
  pool.query('DELETE FROM linker.user WHERE date  + 604800000 < ?', [date]);
  userCheckTime = date;
  return;
}

async function allDebrid(req) {

  let provide = req.session.data.fileProvoder;
  let link = req.session.data.fileLink;
  let freeApiKey = await canProceedFunction(req.session.data.sizeGb, req.session.data.fileProvoder);
  updateApiKeys(freeApiKey, req.session.data.sizeGb, req.session.data.fileProvoder);
  let options = await getAllDebrid(link, freeApiKey);
  html = await rp(options);
  if (html.body) {
    let response = JSON.parse(html.body);
    let data;
    // data.donloadFile = true;
    data = response.data.link;
    req.session.data.fileLink = '';
    req.session.data.download = true;
    req.session.data.premiumLink = data;
    //req.session.data.clickValue = {};

    return;
  }
}



async function getAllDebrid(link, apikey) {
  const options = rp.OptionsWithUri = {
    "uri": 'https://api.alldebrid.com/v4/link/unlock?agent=kradja&apikey=' + apikey.api_key + '&link=' + link,
    "method": "GET",
    resolveWithFullResponse: true
  };
  return options;
}

async function getAllDebridTestLink(link) {
  let key = await canCheckLink();
  const options = rp.OptionsWithUri = {
    "uri": 'https://api.alldebrid.com/v4/link/infos?agent=kradja&apikey=' + key.api_key + '&link[]=' + link,
    "method": "GET",
    resolveWithFullResponse: true
  };
  return options;
}

async function canProceedFunction(sizeGb, provider) {
  let gibPriceTest = {};
  if (gibPrice[provider]) {
    gibPriceTest = gibPrice[provider];
  } else {
    gibPriceTest = gibPrice['default'];
    provider = 'default';
  }
  let now = new Date().getTime();

  for (let i in premiumApiObj) {
    if (now > premiumApiObj[i].reset_timer) {
      premiumApiObj[i].data = null;
    }
    if (premiumApiObj[i].data) {
      if (premiumApiObj[i].data[provider].used + sizeGb > gibPriceTest.limit) {
        return premiumApiObj[i];
      }
    } else {
      return premiumApiObj[i];
    }

  }
  return false;

}

async function canCheckLink() {
  let now = new Date().getTime();
  for (let i in premiumApiObj) {
    if (premiumApiObj[i].data) {
      if (premiumApiObj[i].data.checked) {
        if (premiumApiObj[i].data.checked.reset_timer < now) {
          premiumApiObj[i].data.checked.times = 0;
          premiumApiObj[i].data.checked.reset_timer = now + 60000;
        } else {
          premiumApiObj[i].data.checked.times += 1;
        }
      } else {
        premiumApiObj[i].data.checked = {};
        premiumApiObj[i].data.checked.times = 1;
        premiumApiObj[i].data.checked.reset_timer = now + 60000;
      }
    } else {
      premiumApiObj[i].data  = {};
      premiumApiObj[i].data.checked = {};
      premiumApiObj[i].data.checked.times = 1;
      premiumApiObj[i].data.checked.reset_timer = now + 60000;
    }
    if(premiumApiObj[i].data.checked.times != 60) {
      return premiumApiObj[i];
    }
  }
}

async function updateApiKeys(freeApiKey, size, provider) {

  if (gibPrice[provider]) {
    gibPriceTest = gibPrice[provider];
  } else {
    gibPriceTest = gibPrice['default'];
    provider = 'default';
  }

  if (!freeApiKey.data) {
    freeApiKey.data = {};
    freeApiKey.data[provider] = {};
    freeApiKey.data[provider].used = size;
  } else {
    freeApiKey.data[provider].used += size;
  }
  pool.query(`UPDATE linker.all_debrid SET data = ? where id = ?`, [JSON.stringify(freeApiKey.data), freeApiKey.id]);

}


let wait = ms => new Promise((r, j) => setTimeout(r, ms));


module.exports = router;
